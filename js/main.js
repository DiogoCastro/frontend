$(document).ready(function () {
    var headerNav = $('.header');

    $( ".burger" ).click(function() {
        $(this).toggleClass("active");
        $(".list-nav").toggle();
    });

    $(window).scroll(function () {
        if ($(window).scrollTop() > 250) {
            headerNav.removeClass("not-visible");
        } else {
            headerNav.addClass("not-visible");
        }

        $('.hideme').each(function (i) {
            var bottom_of_object = $(this).position().top + $(this).outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();
            if (bottom_of_window > bottom_of_object - 500) {
                $(this).animate({'opacity': '1'}, 500);
            }
        });
    });
});